package com.senyint.hip.saas.hlht.config;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;
 

/**
 * *******description*******
 * 消息服务相关配置
 * *************************
 *
 * @author ChenPan
 * @date 2018/8/6 16:31
 */
@Configuration
@ConfigurationProperties(prefix = "hip.server")
@Data
public class MessageServerProperties {

    /**
     * 支持的交互操作命令及对应的解析器
     */
   private Map<String, Parser> actions = new HashMap<>();
    
   private String filePath;

    /**
     * 解析器
     */
    @Data
    public static class Parser {

         
        private String sql;
        
        private String  serviceId;
        
        private String schemaName;
        
        private String templateName;
        
        private String messageName;
        
        private  Boolean enabled;
        

      
    }
}
