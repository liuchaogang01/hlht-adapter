package com.senyint.hip.saas.hlht.message.adapter.sdk;

import java.io.IOException;

import java.util.Map;

import org.apache.commons.io.IOUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.senyint.hip.saas.hlht.util.UUIDUtils;
import com.senyint.hip.saas.message.adapter.sdk.MessageConverter;

import com.senyint.hip.saas.message.core.MessageConstant;
import com.senyint.hip.saas.message.core.MessageModel;

/**
 * 
 * *******description******* 
 * 互联互通消息转换
 *  *************************
 * 
 * @author liuchaogang
 * @date 2018年12月29日 上午10:44:30
 *
 */
public class HlHtMessageConverter {

	 

	/**
	 * 
	 * @param schemaName   结构定义文件名称
	 * @param templateName xml模板文件名称
	 * @param msgType      消息类型
	 * @param messageMap   消息Map
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws Exception
	 */
	public static String model2Msg(String schemaName, String templateName, String msgType,
			Map<String, Object> messageMap) throws JsonParseException, JsonMappingException, IOException, Exception {

		String xml = IOUtils.toString(
				MessageConverter.class.getClassLoader().getResourceAsStream("templates/" + templateName + ".xml"));
		String json = IOUtils.toString(
				MessageConverter.class.getClassLoader().getResourceAsStream("schemas/" + schemaName + ".json"));
		
		MessageModel messageModel = new MessageModel();
		messageModel.setContent(messageMap);
		return MessageConverter.model2Msg(json, xml, MessageConstant.MESSAGE_TYPE_V3, messageModel);

	}

}
