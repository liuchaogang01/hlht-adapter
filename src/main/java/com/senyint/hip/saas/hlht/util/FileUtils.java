package com.senyint.hip.saas.hlht.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

/**
 * 
 * *******description*******
 * 
 * *************************
 * 
 * @author liuchaogang
 * @date 2018年12月29日 下午2:41:41
 *
 */
public class FileUtils {

	public static void OutputXml(String xml, String filePath, String filename) throws DocumentException {

		Document doc = DocumentHelper.parseText(xml);
		OutputFormat format = OutputFormat.createPrettyPrint();
		/** 指定XML编码 */
		format.setEncoding("UTF-8");
		/** 将document中的内容写入文件中 */
		XMLWriter writer;
		try {
			File path = new File(filePath);
			if (!path.exists()) {
				path.mkdirs();

			}

			writer = new XMLWriter(new FileWriter(new File(path, filename)), format);
			writer.write(doc);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
