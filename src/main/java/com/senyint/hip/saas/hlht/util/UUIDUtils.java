package com.senyint.hip.saas.hlht.util;

import java.util.UUID;

public class UUIDUtils {
	
 

	 /**
	  * 获取一个不带下划线的UUID
	  * @return
	  */
	  public static String getUUID(){
	        String uuid = UUID.randomUUID().toString();
	        //去掉“-”符号 
	        return uuid.replaceAll("-", "");
	    }
}
