package com.senyint.hip.saas.hlht.service.impl;

 
import java.util.List;
import java.util.Map;

 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

 
import com.senyint.hip.saas.hlht.config.MessageServerProperties;
import com.senyint.hip.saas.hlht.message.adapter.sdk.HlHtMessageConverter;
import com.senyint.hip.saas.hlht.service.HisView4XmlService;
import com.senyint.hip.saas.hlht.util.FileUtils;
import com.senyint.hip.saas.hlht.util.UUIDUtils;
import com.senyint.hip.saas.message.core.MessageConstant;
import com.senyint.hip.saas.mybatis.dao.CommonDao;

@Service
public class HisView4XmlServiceImpl implements HisView4XmlService {

 

	@Autowired
	CommonDao commonDao;

	@Autowired
	MessageServerProperties messageServerProperties;

	public void hisView4Xml(String action) {
	      
		 MessageServerProperties.Parser parser = messageServerProperties.getActions().get(action);
		 
		    if(ObjectUtils.isEmpty(parser)) {
		    	return ;
		    }
		     
		    
		    	 if(parser.getEnabled()) {
		    		 List<Map<String,Object>> messageList = commonDao.query4ListMap(parser.getSql());
		    		 
		    		 for(Map<String,Object> messageMap:messageList) {
		    			 System.out.println(messageMap);
		    			 messageMap.put("id", UUIDUtils.getUUID());
      			  try {
				    FileUtils.OutputXml(HlHtMessageConverter.model2Msg(parser.getSchemaName(),parser.getTemplateName(), MessageConstant.MESSAGE_TYPE_V3,messageMap), messageServerProperties.getFilePath()+parser.getMessageName()+"\\",messageMap.get("id")+".xml");
				} catch (Exception e) {
					 
					e.printStackTrace();
				}  
		    	 
		    		 }
		    	 }
		      
		    
		    
		 
	}

 
 
}
