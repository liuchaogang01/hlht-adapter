package com.senyint.hip.saas;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.senyint.hip.saas.hlht.service.HisView4XmlService;

@SpringBootApplication
public class HlhtApplication implements CommandLineRunner{

	public static void main(String[] args)  {
		SpringApplication.run(HlhtApplication.class, args);
	}

	@Autowired
	HisView4XmlService  hisView4XmlService;
	@Override
	public void run(String... args) throws Exception {
		 
	/*	hisView4XmlService.hisView4Xml("PatientRegistryAddRequest");
		hisView4XmlService.hisView4Xml("PatientRegistryReviseRequest");
		hisView4XmlService.hisView4Xml("AmbulatoryEncounterStarted");
		hisView4XmlService.hisView4Xml("AddActOrder");
		hisView4XmlService.hisView4Xml("AddActRequest");
		hisView4XmlService.hisView4Xml("InpatientEncounterCompleted");
		hisView4XmlService.hisView4Xml("InpatientEncounterStarted");
		hisView4XmlService.hisView4Xml("AddProviderRequest");*/
		hisView4XmlService.hisView4Xml("UpdateProviderRequest");
		System.exit(0);
	}

}

